#include "Datacollection.h"

Collection::Collection(std::list<Contract> Contracts, std::list<Service> Services,
                       std::list<Group> Groups, std::list<Worker> Workers,
                       std::list<Job> Jobs){};

std::list<Contract> Collection::getActiveContracts() {
  std::list<Contract> ActiveContracts;
  for (Contract contract : contracts) {
    if (contract.status == "Active") {
      ActiveContracts.push_back(contract);
    }
  }
  return ActiveContracts;
}