#ifndef UNTITLEDCLASSES_MAINSCREEN_H
#define UNTITLEDCLASSES_MAINSCREEN_H

#include <iostream>

class Mainscreen {
private:
  long id = 0;

public:
  Mainscreen();

  void welcoming_message();
  void available_options();
  int get_user_input();
};

#endif // UNTITLEDCLASSES_MAINSCREEN_H