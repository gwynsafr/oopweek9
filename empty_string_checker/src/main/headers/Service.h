#ifndef UNTITLEDCLASSES_Service_H
#define UNTITLEDCLASSES_Service_H

#include <iostream>
#include <string>
#include <utility>

class Service {
private:
  long id = 0;

public:
  std::string name = "empty";
  double cost = 0.0;
  std::string description = "empty";
  Service(long id, std::string name, double cost, std::string description);

  Service();

  friend std::ostream &operator<<(std::ostream &os, const Service &service) {
    std::cout << service.name << " | " << service.cost << " | "
              << service.description << std::endl;
  };

  const std::string &getName() const { return this->name; };
  void setName(const std::string &newname) { this->name = newname; };

  double getCost() const { return this->cost; };
  void setCost(double newcost) { this->cost = newcost; };

  const std::string &getDescription() const { return this->description; };
  void setDescription(const std::string &newdescription) {
    this->description = newdescription;
  };
};

#endif // UNTITLEDCLASSES_Service_H