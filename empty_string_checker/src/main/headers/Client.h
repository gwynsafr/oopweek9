#ifndef UNTITLEDCLASSES_Client_H
#define UNTITLEDCLASSES_Client_H

#include <iostream>
#include <list>
#include <string>
#include <utility>

#include "Event.h"

class Client {
private:
  long id = 0;
  std::string name = "empty";
  std::string requisites = "empty"; // or smth
  std::string address = "empty";
  std::string phone = "empty";
  std::list<Event> events = {};

public:
  Client(long id, std::string name, std::string requisites, std::string address,
         std::string phone, std::list<Event>);
  Client();

  friend std::ostream &operator<<(std::ostream &os, const Client &client) {
    std::cout << client.name << " | " << client.requisites << " | "
              << client.address << " | " << client.phone << std::endl;
  };

  const std::string &getName() const { return this->name; };
  void setName(const std::string &n) { this->name = n; };

  const std::string &getRequisites() const { return this->requisites; };
  void setRequisites(const std::string &req) { this->requisites = req; };

  const std::string &getAddress() const { return this->address; };
  void setAddress(const std::string &a){this->address = a; };

  const std::string &getPhone() const { return this->phone; };
  void setPhone(const std::string &p) { this->phone = p; };

  const std::list<Event> &getEvents() const { return this->events; };
  void setEvents(const std::list<Event> &e) { this->events = e; };
};

#endif // UNTITLEDCLASSES_Client_H