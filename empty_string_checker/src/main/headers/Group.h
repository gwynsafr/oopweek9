#ifndef UNTITLEDCLASSES_Group_H
#define UNTITLEDCLASSES_Group_H

#include <iostream>
#include <list>
#include <string>
#include <utility>

#include "Worker.h"

class Group {
private:
  long id = 0;

public:
  std::list<Worker> workers = {};
  Group(long id, std::list<Worker> workers);
  Group();

  friend std::ostream &operator<<(std::ostream &os, const Group &group) {
    std::cout << "Group " << group.id << ":\n";
    for (auto worker : group.workers) {
      std::cout << worker << '\n';
    }
  };

  const std::list<Worker> &getWorkers() const { return this->workers; };
  void setWorkers(const std::list<Worker> &newworkers) {
    this->workers = newworkers;
  };
};

#endif // UNTITLEDCLASSES_Group_H