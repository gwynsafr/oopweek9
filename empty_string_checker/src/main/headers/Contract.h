#ifndef UNTITLEDCLASSES_Contract_H
#define UNTITLEDCLASSES_Contract_H

#include <iostream>
#include <list>
#include <string>
#include <utility>

#include "Client.h"
#include "Service.h"
#include "Group.h"

#include "Client.cpp"

class Contract {
private:
  long id = 0;

public:
  Client client;
  std::list<Service> services = {};
  std::string contract_start_date = "0.0.0";
  std::string contract_end_date = "0";
  std::list<Group> groups;
  std::string status = "Canceled";

  Contract(long id, Client client, std::list<Service> services, std::string contract_start_date,
           std::string contract_end_date, std::list<Group> groups, std::string status);
  Contract();

  friend std::ostream &operator<<(std::ostream &os, const Contract &contract) {
    std::cout << contract.id << " | " << contract.client << " | "
              << contract.status << std::endl;
  };

  const Client &getClient() const { return this->client; };
  void setClient(const Client &clnt) { this->client = clnt; };

  const std::list<Service> &getServices() const { return this->services; };
  void setService(const std::list<Service> &servs) { this->services = servs; };

  const std::string &getContractStartDate() const {
    return this->contract_start_date;
  };
  void setContractStartDate(const std::string &contrct_start_date) {
    this->contract_start_date = contrct_start_date;
  };

  const std::string &getContractEndDate() const {
    return this->contract_end_date;
  };
  void setContractEndDate(const std::string &contrct_end_date) {
    this->contract_end_date = contrct_end_date;
  };

  const std::list<Group> &getGroups() const { return this->groups; };
  void setGroups(const std::list<Group> &grps) { this->groups = grps; };
};

#endif // UNTITLEDCLASSES_Contract_H