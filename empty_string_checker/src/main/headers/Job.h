#ifndef UNTITLEDCLASSES_JOB_H
#define UNTITLEDCLASSES_JOB_H

#include <iostream>
#include <string>
#include <utility>

class Job {
private:
  long id = 0;

public:
  std::string name = "empty";
  double salary = 0.0;
  Job(long id, std::string name, double salary);
  Job();

  friend std::ostream &operator<<(std::ostream &os, const Job &job) {
    std::cout << job.name << " | " << job.salary << std::endl;
  };

  const std::string &getName() const { return this->name; };
  void setName(const std::string &newname) { this->name = newname; };

  const double &getSalary() const { return this->salary; };
  void setSalary(const double &newsalary) { this->salary = newsalary; };
};

#endif // UNTITLEDCLASSES_JOB_H