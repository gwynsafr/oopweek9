#ifndef UNTITLEDCLASSES_Event_H
#define UNTITLEDCLASSES_Event_H

#include <ostream>
#include <string>
#include <utility>

#include "Group.h"

class Event {
private:
  long id = 0;

public:
  std::string date = "0.0.0";
  Group group;
  std::string description = "empty";
  Event();
  Event(long id, std::string date, Group group,
        std::string description);

  friend std::ostream &operator<<(std::ostream &os, const Event &event) {
    std::cout << event.id << " | " << event.date << " | "
              << event.group << " | " << event.description << std::endl;
  };

  const Group &getGroup() const { return this->group; };
  void setGroup(const Group &dt) { this->group = dt; };

  const std::string &getDescription() const { return this->description; };
  void setDescription(const std::string &descript) {
    this->description = descript;
  };
};

#endif // UNTITLEDCLASSES_Event_H