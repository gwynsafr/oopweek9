#ifndef UNTITLEDCLASSES_COLLECTION_H
#define UNTITLEDCLASSES_COLLECTION_H

#include <iostream>
#include <list>
#include <string>
#include <utility>

#include "Contract.h"
#include "Service.h"
#include "Group.h"
#include "Worker.h"
#include "Job.h"

class Collection {
private:
  long id = 0;

public:
  std::list<Contract> contracts;
  std::list<Service> services;
  std::list<Group> groups;
  std::list<Worker> workers;
  std::list<Job> jobs;
  Collection(std::list<Contract> Contracts, std::list<Service> Services,
             std::list<Group> Groups, std::list<Worker> Workers,
             std::list<Job> Jobs);

  std::list<Contract> getActiveContracts();
};

#endif // UNTITLEDCLASSES_COLLECTION_H